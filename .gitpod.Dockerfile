FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y libgtk-3-dev && \ 
    sudo apt-get install -y libgl1-mesa-dev xorg-dev && \
    sudo rm -rf /var/lib/apt/lists/*
