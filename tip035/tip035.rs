// rustc tip035.rs
// ./tip035

fn main() {
    let z = String::from("World");
    println!("Address of z: {:p}", &z);                  // Address of z: 0x7ffcd9097f60
    println!("Address of 'World': {:p}", z.as_ptr());    // Address of 'World': 0x5556d5a25c90
}
