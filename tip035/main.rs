// rustc main.rs
// ./main

fn main() {
    let x = 12;
    //NG: println!(x);          // error: format argument must be a string literal
    println!("{}", x);          // 12
    println!("{}", &x);         // 12
    println!("{:p}", &x);       // 0x7ffccbc47c14
    //NG: println!("{}", *x);   // error[E0614]: type `{integer}` cannot be dereferenced

    let a = &x;
    println!("{}", a);          // 12
    println!("{}", &a);         // 12
    println!("{:p}", &a);       // 0x7ffccbc47eb0
    println!("{}", *a);         // 12

    let y = "Hello";
    //NG: println!(y);           // error: format argument must be a string literal
    println!("{}", y);           // Hello
    println!("{}", &y);          // Hello
    println!("{:p}", &y);        // 0x7ffccbc47ce8
    //NG: println!("{}", *y);    // error[E0277]: the size for values of type `str` cannot be known at compilation time

    let b= &y;
    println!("{}", b);          // 12
    println!("{}", &b);         // 12
    println!("{:p}", &b);       // 0x7ffccbc47eb0
    println!("{}", *b);         // 12

    let z = String::from("World");
    //NG: println!(z);        // error: format argument must be a string literal
    println!("{}", z);        // World
    println!("{}", &z);       // World
    println!("{:p}", &z);     // 0x7ffccbc47dc8
    // println!("{}", *z);    // error[E0277]: the size for values of type `str` cannot be known at compilation time

    let c = &z;
    println!("{}", c);          // 12
    println!("{}", &c);         // 12
    println!("{:p}", &c);       // 0x7ffccbc47eb0
    println!("{}", *c);         // 12
}
