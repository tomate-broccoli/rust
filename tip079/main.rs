// rustc main.rs
// ./main

fn main() {
    let s1: String = String::from("Hello, World!");
    println!(" s1: {}", s1); 
    println!("&s1: {}", &s1);
    //NG: println!("*s1: {}", *s1);    // help: the trait `Sized` is not implemented for `str`

    let s2: &str = &s1;
    println!(" s2: {}", s2);
    println!("&s2: {}", &s2);

    let s3: String = s2.to_string();
    println!(" s3: {}", s3);
    println!("&s3: {}", &s3);
}
