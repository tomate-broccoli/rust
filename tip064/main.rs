// rustc main.rs
// ./main

// fn as_str(data: &u32) -> &str {
//     let s = format!("{}", data);
//     &s                              // error[E0515]: cannot return reference to local variable `s`
// }
fn as_str<'a>(data: &'a u32) -> Box<str> {
    let s = format!("{}", data);
    s.into_boxed_str()              // Box<str> に変換して返す
}

fn main() {
    let res = as_str(&123);
    println!("{res}");
}
