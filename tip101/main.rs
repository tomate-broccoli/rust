// rustc main.rs
// ./main

fn main() {
    let array: [i32; 3] = [1, 2, 3];
    println!("array: {:?}", array);      // array: [1, 2, 3]
    println!("&array: {:p}", &array);    // &array: 0x7fff8fa0b63c

    let slice: &[i32] = &array;
    println!("slice: {:?}", slice);      // slice: [1, 2, 3]
    println!("slice: {:p}", slice);      // slice: 0x7fff8fa0b63c
    println!("&slice: {:p}", &slice);    // &slice: 0x7fff8fa0b6d0

    println!("&*slice: {:?}", &*slice);  // &*slice: [1, 2, 3]
    println!("&*slice: {:p}", &*slice);  // &*slice: 0x7fff8fa0b63c
}