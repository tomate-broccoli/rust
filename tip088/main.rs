use std::any::type_name;

// rustc main.rs
// ./main

fn print_type_of<T>(_: &T) {
    println!("type: {}.", type_name::<T>());
}

fn main() {
    let x = 10;
    let y = "Hello";
    let z = vec![1, 2, 3];

    print_type_of(&x);   // type: i32.
    print_type_of(&y);   // type: &str.
    print_type_of(&z);   // type: alloc::vec::Vec<i32>.
}
